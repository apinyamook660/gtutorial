import timeit


def fibonac_recur(n):
    if n in [0, 1]:
        return n
    return fibonac_recur(n - 1) + fibonac_recur(n - 2)


def fibonac_b2u(n):
    f0 = 0
    f1 = 1
    f2 = 1

    for i in range(2, n + 1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    print(
        "fibonac_recur: ",
        fibonac_recur(10),
        timeit.timeit(
            "fibonac_recur(10)",
            globals=globals(),
        ),
    )
    print(
        "finonac_b2u: ",
        fibonac_b2u(10),
        timeit.timeit("fibonac_b2u(10)", globals=globals()),
    )
