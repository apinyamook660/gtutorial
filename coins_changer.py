def coins_changer(money):
    coins = [10, 5, 4, 2, 1]
    c = [0, 0, 0, 0, 0]
    for i in range(len(coins)):
        if money >= coins[i]:
            c[i] = money // coins[i]
            money = money % coins[i]
    return c, coins


if __name__ == "__main__":
    money = int(input("Enter your money: "))
    all_coins, coins = coins_changer(money)
    for i, j in zip(coins, all_coins):
        print("Coins {} is {}".format(i, j))
